package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
)

// Server stores the state between every client
type Server struct {
	clients  []*Session
	joins    chan net.Conn
	incoming chan string
}

// NewServer constructs and starts a new Server
func NewServer() *Server {
	svr := &Server{
		clients:  make([]*Session, 0),
		joins:    make(chan net.Conn),
		incoming: make(chan string),
	}

	svr.Listen()
	return svr
}

// Join threats a join requisition from clients to the Server state
func (svr *Server) Join(connection net.Conn) {

	client := NewSession(connection)
	svr.clients = append(svr.clients, client)

	go func() {
		for {
			svr.incoming <- <-client.incoming
		}
	}()
}

// Listen receives incoming messagens and new connections from clients
func (svr *Server) Listen() {
	go func() {
		for {
			select {
			case data, ok := <-svr.incoming:
				if !ok {
					return
				}
				fmt.Println(data)

			case conn := <-svr.joins:
				svr.Join(conn)
			}
		}
	}()
}

// Shutdown realeases every resource and finishes goroutines launched by the server
func (svr *Server) Shutdown() {

	for _, v := range svr.clients {
		v.Disconnect()
	}
	close(svr.joins)
	close(svr.incoming)
}

var svrID string
var svrPort string

func init() {
	flag.StringVar(&svrID, "id", "", "Set server unique ID")
	flag.StringVar(&svrPort, "port", ":11000", "Set the service server bind address")
}

func main() {

	flag.Parse()
	if svrID == "" {
		log.Fatalln("must set a server ID, run with: ./server -id 'svrID'")
	}

	// Initialize the Chat Server
	service := NewServer()
	listener, err := net.Listen("tcp", svrPort)
	if err != nil {
		log.Fatalf("failed to start connection: %s", err.Error())
	}

	go func() {
		for {
			conn, err := listener.Accept()
			if err != nil {
				log.Fatalf("accept failed: %s", err.Error())
			}
			service.joins <- conn
		}
	}()

	terminate := make(chan os.Signal, 1)
	signal.Notify(terminate, os.Interrupt)
	<-terminate
	service.Shutdown()
}
