package main

import (
	"math/rand"
	"strconv"
	"sync"
	"testing"
	"time"
)

const (
	numClients  = 3
	numMessages = 10
)

func TestOrderReq(t *testing.T) {

	configBarrier := new(sync.WaitGroup)
	configBarrier.Add(numClients)

	finishedBarrier := new(sync.WaitGroup)
	finishedBarrier.Add(numClients)

	clients := make([]*Info, numClients, numClients)

	for i := 0; i < numClients; i++ {
		go func(j int) {

			var err error
			clients[j], err = New("config.toml")
			if err != nil {
				t.Fatalf("failed to find config: %s", err.Error())
			}

			err = clients[j].Connect()
			if err != nil {
				t.Fatalf("failed to connect to cluster: %s", err.Error())
			}

			// Wait until all goroutines finish configuration
			configBarrier.Done()
			configBarrier.Wait()

			for k := 0; k < numMessages; k++ {

				msg := strconv.Itoa(j) + "-" + strconv.Itoa(k) + "-" + strconv.Itoa(rand.Intn(100)) + "\n"
				clients[j].Broadcast(msg)

				// Avoid Burst requisitions. Could ve been done waiting for servers repply, but avoided
				// that logic for the sake of simplicity
				time.Sleep(500 * time.Millisecond)
			}
			finishedBarrier.Done()
		}(i)
	}
	finishedBarrier.Wait()

	for _, v := range clients {
		v.Disconnect()
	}
}
