#!/bin/bash

apt-get update
yes | apt-get install golang
export GOPATH=~/go
export GOBIN=$GOPATH/bin
go get github.com/BurntSushi/toml
go get github.com/hashicorp/raft

echo "finished installing depencies"