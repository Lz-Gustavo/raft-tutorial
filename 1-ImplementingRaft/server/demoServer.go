package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"

	"github.com/hashicorp/raft"
)

// Server stores the state between every client
type Server struct {
	clients  []*Session
	joins    chan net.Conn
	incoming chan string

	raft *raft.Raft
}

// NewServer constructs and starts a new Server
func NewServer() *Server {
	svr := &Server{
		clients:  make([]*Session, 0),
		joins:    make(chan net.Conn),
		incoming: make(chan string),
	}

	svr.Listen()

	if joinHandlerAddr != "" {
		svr.ListenRaftJoins(joinHandlerAddr)
	}
	return svr
}

// Join threats a join requisition from clients to the Server state
func (svr *Server) Join(connection net.Conn) {

	client := NewSession(connection)
	svr.clients = append(svr.clients, client)

	go func() {
		for {
			svr.incoming <- <-client.incoming
		}
	}()
}

// Listen receives incoming messagens and new connections from clients
func (svr *Server) Listen() {
	go func() {
		for {
			select {
			case data, ok := <-svr.incoming:
				if !ok {
					return
				}
				fmt.Println(data)
				// TODO: ...

			case conn := <-svr.joins:
				svr.Join(conn)
			}
		}
	}()
}

// Shutdown realeases every resource and finishes goroutines launched by the server
func (svr *Server) Shutdown() {

	for _, v := range svr.clients {
		v.Disconnect()
	}
	close(svr.joins)
	close(svr.incoming)
}

var svrID, svrPort, raftAddr string
var joinAddr, joinHandlerAddr string

func init() {
	flag.StringVar(&svrID, "id", "", "Set server unique ID")
	flag.StringVar(&svrPort, "port", ":11000", "Set the service server bind address")

	flag.StringVar(&raftAddr, "raft", ":12000", "Set RAFT consensus bind address")
	flag.StringVar(&joinAddr, "join", "", "Set join address, if any")
	flag.StringVar(&joinHandlerAddr, "hjoin", "", "Set port id to receive join requests on the raft cluster")

}

func main() {

	flag.Parse()
	if svrID == "" {
		log.Fatalln("must set a server ID, run with: ./server -id 'svrID'")
	}

	// Initialize the Chat Server
	service := NewServer()
	listener, err := net.Listen("tcp", svrPort)
	if err != nil {
		log.Fatalf("failed to start connection: %s", err.Error())
	}

	// Start the Raft cluster
	if err := service.StartRaft(joinAddr == "", svrID, raftAddr); err != nil {
		log.Fatalf("failed to start raft cluster: %s", err.Error())
	}

	// Send a join request, if any
	if joinAddr != "" {
		if err = sendJoinRequest(); err != nil {
			log.Fatalf("failed to send join request to node at %s: %s", joinAddr, err.Error())
		}
	}

	go func() {
		for {
			conn, err := listener.Accept()
			if err != nil {
				log.Fatalf("accept failed: %s", err.Error())
			}
			service.joins <- conn
		}
	}()

	terminate := make(chan os.Signal, 1)
	signal.Notify(terminate, os.Interrupt)
	<-terminate
	service.Shutdown()
}

// StartRaft opens the store. If enableSingle is set, and there are no existing peers,
// then this node becomes the first node, and therefore leader of the cluster.
// localID should be the server identifier for this node.
func (svr *Server) StartRaft(enableSingle bool, localID string, localRaftAddr string) error {

	// TODO: ...
	return nil
}

// JoinRaft joins a raft node, identified by nodeID and located at addr
func (svr *Server) JoinRaft(nodeID, addr string) error {

	configFuture := svr.raft.GetConfiguration()
	if err := configFuture.Error(); err != nil {
		return err
	}

	for _, rep := range configFuture.Configuration().Servers {

		// If a node already exists with either the joining node's ID or address,
		// that node may need to be removed from the config first.
		if rep.ID == raft.ServerID(nodeID) || rep.Address == raft.ServerAddress(addr) {

			// However if *both* the ID and the address are the same, then nothing -- not even
			// a join operation -- is needed.
			if rep.Address == raft.ServerAddress(addr) && rep.ID == raft.ServerID(nodeID) {
				return nil
			}

			future := svr.raft.RemoveServer(rep.ID, 0, 0)
			if err := future.Error(); err != nil {
				return fmt.Errorf("error removing existing node %s at %s: %s", nodeID, addr, err)
			}
		}
	}

	f := svr.raft.AddVoter(raft.ServerID(nodeID), raft.ServerAddress(addr), 0, 0)
	if f.Error() != nil {
		return f.Error()
	}
	return nil
}

// ListenRaftJoins receives incoming join requests to the raft cluster. Its initialized
// when "-hjoin" flag is specified, and it can be set only in the first node in case you
// have a static/imutable cluster architecture
func (svr *Server) ListenRaftJoins(addr string) {

	go func() {
		listener, err := net.Listen("tcp", addr)
		if err != nil {
			log.Fatalf("failed to bind connection at %s: %s", addr, err.Error())
		}

		for {
			conn, err := listener.Accept()
			if err != nil {
				log.Fatalf("accept failed: %s", err.Error())
			}

			request, _ := bufio.NewReader(conn).ReadString('\n')

			request = strings.TrimSuffix(request, "\n")
			data := strings.Split(request, "-")
			if len(data) < 2 {
				log.Fatalf("incorrect join request, data: %s", data)
			}

			err = svr.JoinRaft(data[0], data[1])
			if err != nil {
				log.Fatalf("failed to join node at %s: %s", data[1], err.Error())
			}
		}
	}()
}

func sendJoinRequest() error {

	joinConn, err := net.Dial("tcp", joinAddr)
	if err != nil {
		return fmt.Errorf("failed to connect to leader node at %q: %s", joinAddr, err.Error())
	}
	_, err = fmt.Fprint(joinConn, svrID+"-"+raftAddr+"\n")
	if err != nil {
		return fmt.Errorf("failed to send join request to node at %q: %s", joinAddr, err.Error())
	}
	if err = joinConn.Close(); err != nil {
		return err
	}
	return nil
}
