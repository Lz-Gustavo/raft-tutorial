package main

import (
	"fmt"
	"io"

	"github.com/hashicorp/raft"
)

type fsm Server

func (f *fsm) Apply(l *raft.Log) interface{} {
	fmt.Println(string(l.Data))
	return true
}

func (f *fsm) Snapshot() (raft.FSMSnapshot, error) {
	return &fsmSnapshot{save: "hello"}, nil
}

func (f *fsm) Restore(rc io.ReadCloser) error {
	return nil
}

type fsmSnapshot struct {
	save string
}

func (f *fsmSnapshot) Persist(sink raft.SnapshotSink) error {
	return nil
}

func (f *fsmSnapshot) Release() {}
